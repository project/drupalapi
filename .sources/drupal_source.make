core = 7.x
api = 2



; Libraries

libraries[documentation-6.x][download][type] = git
libraries[documentation-6.x][download][url] = http://git.drupal.org/project/documentation.git
libraries[documentation-6.x][download][branch] = 6.x-1.x
libraries[documentation-6.x][destination] = .sources
libraries[documentation-6.x][api_project_name] = drupal
libraries[documentation-6.x][api_branch] = 6.x

libraries[documentation-7.x][download][type] = git
libraries[documentation-7.x][download][url] = http://git.drupal.org/project/documentation.git
libraries[documentation-7.x][download][branch] = 7.x-1.x
libraries[documentation-7.x][destination] = .sources
libraries[documentation-7.x][api_project_name] = drupal
libraries[documentation-7.x][api_branch] = 7.x

libraries[drupal-6.x][download][type] = git
libraries[drupal-6.x][download][url] = http://git.drupal.org/project/drupal.git
libraries[drupal-6.x][download][branch] = 6.x
libraries[drupal-6.x][destination] = .sources
libraries[drupal-6.x][api_project_name] = drupal

libraries[drupal-7.x][download][type] = git
libraries[drupal-7.x][download][url] = http://git.drupal.org/project/drupal.git
libraries[drupal-7.x][download][branch] = 7.x
libraries[drupal-7.x][destination] = .sources
libraries[drupal-7.x][api_project_name] = drupal
libraries[drupal-7.x][api_project_title] = Drupal
libraries[drupal-7.x][api_project_type] = core
libraries[drupal-7.x][api_preferred] = true
